# Data Science Essentials Course Development Tool Setup

Indiana University's Data Science Essentials course contains many programming examples and exercises.
Course Link: https://expand.iu.edu/browse/sice/skillsets/programs/dse

## Development System Configurations

This repository captures personal notes for installing and configuring development enviroments for completing coursework.
Development tools were installed on several computer system configurations.

| Documentation | Operating System Version | System Description |
|+--------------|+-------------------------|+-------------------|
| [README.macos.md](./docs/README.macos.md) | MacOS Ventura 13.4 | 14" Macbook Pro M1 (2021) with Apple's ARM-based M1 silicon. |
| [README.rhel.md](./docs/README.rhel.md) | Redhat Linux 8.6 | Custom AMD Ryzen 5950X / Socket AM4 X570 based system |
| [README.win11.md](./docs/README.win11.md) | Windows 11 | Custom Intel Core i9-12900k / LGA1700 Z690 based system |
