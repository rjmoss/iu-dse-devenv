# .bash_profile
# Redhat Starts new shells as login shells
# So it sources this instead of bashrc
_self=$(basename ${BASH_SOURCE[0]})
echo "--> ${_self}:"
_CFG_PATH=~/repositories/gitlab/iu-dse-devenv/samples/rhel8.6/bash_custom_config
_CFG_FILE=bashrc_custom

# Custom bash config
if [ -f ${_CFG_PATH}/${_CFG_FILE} ]; then
	. ${_CFG_PATH}/${_CFG_FILE}
fi
