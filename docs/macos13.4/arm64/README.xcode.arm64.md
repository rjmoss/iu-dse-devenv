# XCode Command Line Developer Tools
- Link: https://developer.apple.com/support/xcode/
- Link: https://developer.apple.com/xcode/features/

## Notes

XCode provides base development support for MAC OS.  This includes base C language support, GNU tools and version control.

## Install XCode command line tools
- From a terminal prompt
```bash
xcode-select --install
```

- Follow the pop-up prompts to install the developer tools

- Show the install location
```bash
xcode-select -p
	/Library/Developer/CommandLineTools
```

```bash
/Library/Developer/CommandLineTools
├── Library
│   ├── Developer
│   ├── Frameworks
│   └── PrivateFrameworks
├── SDKs
│   ├── MacOSX.sdk -> MacOSX13.3.sdk
│   ├── MacOSX12.3.sdk
│   ├── MacOSX12.sdk -> MacOSX12.3.sdk
│   ├── MacOSX13.3.sdk
│   └── MacOSX13.sdk -> MacOSX13.3.sdk
└── usr
    ├── bin
    ├── include
    ├── lib
    ├── libexec
    └── share

17 directories, 0 file
```