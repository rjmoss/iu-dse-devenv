# Source Controlled Project Quickstart (arm64)

## Open a new Terminal Shell
- This will default to native mode (arm64) on M1/M2 systems
- Use the previously configured alias ```arm64term``` to explicitly launch an arm64 shell
```bash
arm64term
(arm64-bash)dxindustries@~> 
```

## From base source (under source control)
- Base source
```bash
├── .git
├── .gitignore
├── .tool-versions
├── numpy_test.py
└── requirements.txt

2 directories, 4 files
```

- Check the tool version
```bash
cat .tool-versions
	python 3.11.4
```

- Create a virtual environment (named ```env```)
```bash
python -m venv env
```

- Activate the virtual environment
```bash
source env/bin/activate
```

- Check the tool version
```bash
which python
	/Users/dxindustries/tmp/python3_project/env/bin/python
```

- List base packages in the environment
```bash
pip list
	Package    Version
	---------- -------
	pip        23.1.2
	setuptools 65.5.0
```

- Install required packages from ```requirements.txt```
```bash
pip install -r requirements.txt
	Collecting numpy==1.25.1 (from -r requirements.txt (line 1))
	  Using cached numpy-1.25.1-cp311-cp311-macosx_11_0_arm64.whl (14.0 MB)
	Installing collected packages: numpy
	Successfully installed numpy-1.25.1
```

- Call python to test the program
```bash
python -m numpy_test
	[  33.    16.   657.   295.  7197.    23.5  140. ]
	[  -30.   -16.  -700.  -330. -7386.   -23.  -140.]
	7
	18
```

- Deactivate virtual environment
```bash
deactivate
```


