# Mac OSX Native Mode (arm64) Command Line Aliases

## Notes

Homebrew manages installed packages separately from OS (or "system") package installations.  

Even though an M1/M2-based Apple computer is running natively in ARM64 mode, 
we will setup an alias anyway allowing you to launch a new arm64 mode bash session from the terminal.

## Setup Shell Aliases for Native ARM mode
- See: https://stackoverflow.com/questions/75942145/how-to-quickly-change-between-arm64-or-x86-architecture-in-m1-m2-mac-terminals

- Set bashrc to include a special alias configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_aliases ]; then
		. ~/.bash_aliases
	fi
```

- Set aliases in current shell
```bash
alias arm64term='/usr/bin/arch -arm64 /bin/bash --login'
```

- Check settings
```bash
cat ~/.bash_aliases
	# Grab the shell from the current environment (should be /bin/bash)
	_shell=$SHELL

	# Alias for switching shell to Intel x86 mode emulation
	# Note: This has the same effect as running Terminal.app in Rosetta mode

	# ARM Mode
	#alias arm64term="$env /usr/bin/arch -arm64 ${_shell} --login"
	alias arm64term="/usr/bin/arch -arm64 ${_shell} --login

	echo "${_self}: use 'arm64term' to switch to Native arm64 compatible terminal mode."
```

- Check current shell architecture
```bash
arch
	arm64
```

- Launch Native (arm64) shell
```bash
arm64term
```

- Re-check current shell architecture
```bash
arch
	arm64
```