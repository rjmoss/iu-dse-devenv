# ASDF Runtime Version Manager
- Link: https://asdf-vm.com/

## Notes

ASDF manages parallel installations of programming languages.
ASDF can be used with existing language specific library managers for each language.

For Data Science we will use ASDF plugins for Python and R version management that will allow us to use VIRTUALENV and RENV for managing our Python and R versions on our development system.

Use this within Homebrew.

## Install and Configure ASDF with homebrew
- Install asdf dependencies
```bash
brew install curl git
```

- Install asdf
```bash
brew install asdf
```

- Set path to use asdf from command line
```bash
source "$(brew --prefix asdf)/libexec/asdf.sh"
```

- Setup bash completion for asdf
```bash
source "$(brew --prefix asdf)/etc/bash_completion.d/asdf.bash"
```

- Set bashrc to include a special asdf configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_homebrew_asdf ]; then
		. ~/.bash_homebrew_asdf
	fi
```
- Check settings
```bash
cat ~/.bash_homebrew_asdf
# Homebrew ASDF Package Configuration
# Check ARCH support to setup ASDF config variables
if [ $(arch) == "arm64" ]; then
	echo "${_self}: using arm64 architecture"
	ASDF_CFG_SUFFIX="-arm64"
	# Do NOT Override the default tool version filename for arm64 mode
	#export ASDF_DEFAULT_TOOL_VERSIONS_FILENAME=.tool-versions

	# Set ASDF Configuration for running architecture mode
	export ASDF_CONFIG_FILE=${HOME}/.asdfrc${ASDF_CFG_SUFFIX}
	export ASDF_DATA_DIR=${HOME}/.asdf${ASDF_CFG_SUFFIX}
	export ASDF_DIR=$(brew --prefix asdf)/libexec

	# ASDF
	if [ -e "/opt/homebrew/opt/asdf/libexec/asdf.sh" ]; then
		echo "${_self}: using arm64 asdf"
		source "/opt/homebrew/opt/asdf/libexec/asdf.sh"
	else
		echo "${_self}: no arm64 asdf detected!"
	fi
fi
```

- Verify path
```bash
which asdf
	/usr/local/opt/asdf/libexec/bin/asdf
```

