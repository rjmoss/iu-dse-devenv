# Homebrew package management with MAC OS
- Link: https://brew.sh/
- Package Browser: https://formulae.brew.sh/

## Notes

Homebrew manages installed packages separately from OS (or "system") package installations.  There are separate versions required for packages that rely on x86_64 architecture (since the M1 is ARM).

## Setup Shell Aliases for Intel and ARM Support
- See: https://stackoverflow.com/questions/75942145/how-to-quickly-change-between-arm64-or-x86-architecture-in-m1-m2-mac-terminals


## Install and Configure Homebrew
- Install for the native system (arm64)
```bash
arch -arm64 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

- Set bashrc to include a special homebrew configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_homebrew ]; then
		. ~/.bash_homebrew
	fi
```

- Set path in current shell to use homebrew from command line
```bash
eval "$(/usr/local/bin/brew shellenv)"
```

- Check settings
```bash
cat ~/.bash_homebrew
	# Homebrew Path
	# Check ARCH supprot
	if [ $(arch) == "arm64" ]; then
		if [ -e "/opt/homebrew/bin/brew" ]; then
			echo "${_self}: using arm64 homebrew"
			eval "$(/opt/homebrew/bin/brew shellenv)"
		else
			echo "${_self}: no arm64 homebrew detected!"
		fi
	fi
```

- Verify path
```bash
which brew
	/usr/local/bin/brew
```

## Update Homebrew
```bash
brew update
```

## Show installed packages (formulae)
```bash
brew list
	==> Formulae
	asdf		ca-certificates	git		libnghttp2	libyaml		openldap	readline	xz
	autoconf	coreutils	gmp		libssh2		lz4		openssl@1.1	rtmpdump	yq
	automake	curl		jq		libtool		m4		openssl@3	unixodbc	zstd
	brotli		gettext		libidn2		libunistring	oniguruma	pcre2		wget
```

## Uninstall Only Homebrew Formulas (packaged)
```bash
brew remove --force $(brew list --formula)
```

```bash
brew remove --cask --force $(brew list)
```

## Uninstall Homebrew
```bash
arch -arm64 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/uninstall.sh)"
```