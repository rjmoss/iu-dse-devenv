# ASDF Runtime Version Manager
- Link: https://asdf-vm.com/

## Notes

ASDF manages parallel installations of programming languages.
ASDF can be used with existing language specific library managers for each language.

For Data Science we will use ASDF plugins for Python and R version management that will allow us to use VENV (formerly VIRTUALENV in Python 2.x) and RENV for managing our Python and R versions on our development system.

Use this within Homebrew.

## Install and Configure Plugins
- Python Plugin
- Link: https://github.com/asdf-community/asdf-python

### Python 3
- Install system dependencies (including for pyenv)
- Link: https://github.com/pyenv/pyenv/wiki#suggested-build-environment

- Install Python 3 system dependencies in brew
```bash
brew install openssl readline sqlite3 xz zlib tcl-tk pkgconfig
```

- Check for python plugin
```bash
asdf plugin-list-all | grep python
	python                        https://github.com/danhper/asdf-python.git
```

- Install Python Plugin
```bash
asdf plugin-add python
```

- List Python Versions
```bash
asdf list all python 3.
	...
	3.11.0
	3.11-dev
	3.11.1
	3.11.2
	3.11.3
	3.11.4
	3.12.0b3
	3.12-dev
	3.13-dev
```

- Show latest stable Python Version
```bash
asdf latest python 3
	3.11.4
```

### Install A Specific Python Version

#### Example: 3.11.4 for latest (as of 7/4/23)
- Note: Attempts to install normally fail (missing CFLAGS for tcl-tk package)
```bash
env PYTHON_CONFIGURE_OPTS="--enable-shared" asdf install python 3.11.4
	...
	python-build: use zlib from xcode sdk
	^[Traceback (most recent call last):
	  File "<string>", line 1, in <module>
	  File "/Users/dxindustries/.asdf_arm64/installs/python/3.11.4/lib/python3.11/tkinter/__init__.py", line 38, in <module>
	    import _tkinter # If this fails your Python may not be configured for Tk
	    ^^^^^^^^^^^^^^^
	ModuleNotFoundError: No module named '_tkinter'
	WARNING: The Python tkinter extension was not compiled and GUI subsystem has been detected. Missing the Tk toolkit?
	Installed Python-3.11.4 to /Users/dxindustries/.asdf_arm64/installs/python/3.11.4
```

- Use brew pkg-config get CFLAGS to point to the tcl-tk package
- Make sure tcl-tk is installed and pkgconfig
```bash
brew install tcl-tk pkgconfig
```

- Uninstall previously attempted install
```bash
adsf uninstall python 3.11.4
```

- Install Python 3.11.4 with CFLAGS from tcl-tk package
```bash
env PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='$(pkg-config tk --cflags)' --with-tcltk-libs='$(pkg-config tk --libs)' --enable-shared" asdf install python 3.11.4
```

- List Installed Python Versions
```bash
asdf list python
  3.11.4
```

#### Example: 3.8.x for Vitis AI 3.5 Release
- Find the latest 3.8 version
```bash
asdf latest python 3.8
	3.8.17
```

- Install python 3.8.17 with system tcl-tk version
- Note: Even though there is no "issue" installing without the brew tcl-tk installation, there are issues with MacOS Cocoa backend
- Note: Specifying to use the "system" (a.k.a. brew installed) tcl-tk resolves this problem and prevents issues with tkinter drawing black windows
```bash
env PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='$(pkg-config tk --cflags)' --with-tcltk-libs='$(pkg-config tk --libs)' --enable-shared" asdf install python 3.8.17
```

- List Installed Python Versions
```bash
asdf list 
python
  3.11.4
  3.8.17
```

- Set default global version
```bash
asdf global python 3.11.4
```

- Check default global version
```bash
asdf list
python
 *3.11.4
  3.8.17
```

- Check that default global version is set in tool version file
```bash
cat ~/.tool-versions
	python 3.11.4
```

### Verify Tkinter can generate a readable window
- Note: On MacOSX, Tkinter generated a window with black text on a black background
- Note: This was due to not using the correct ```tcl-tk``` version when installing python
- Added ```PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='$(pkg-config tk --cflags)' --with-tcltk-libs='$(pkg-config tk --libs)'``` to the install command

- Launch Python
```bash
python
```

- Import tkinter and run the built-in test to generate a dialog box
- Note: Verify that the dialog is readable
```python
>>> import tkinter
>>> tkinter._test()
```

## Optional

### Set Global Python Environment Configuration
- Set bashrc to include a special homebrew configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_pythonenv ]; then
		. ~/.bash_pythonenv
	fi
```

- Disable Python writing of compiled bytecode files
- Note: In Python3 these are ```.pyc``` files generated in a ```__py_cache__`` folder in the same directory as the module source
- Check settings
```bash
cat ~/.bash_pythonenv
	# Disable Python Bytecode Files
	export PYTHONDONTWRITEBYTECODE=1
```
