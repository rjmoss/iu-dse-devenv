# Mac OSX Bash Command Prompt (show x86_64 Mode)
- Link: https://brew.sh/
- Package Browser: https://formulae.brew.sh/

## Notes

The bash prompt is controlled through the environment variable ```PS1```

## Bash Prompt Configuration Options
```
# Options:
# --------
# \a – A bell character
# \d – Date (day/month/date)
# \D{format} – Use this to call the system to respond with the current time
# \e – Escape character
# \h – Hostname (short)
# \H – Full hostname (domain name)
# \j – Number of jobs being managed by the shell
# \l – The basename of the shells terminal device
# \n – New line
# \r – Carriage return
# \s – The name of the shell
# \t – Time (hour:minute:second)
# \@ – Time, 12-hour AM/PM
# \A – Time, 24-hour, without seconds
# \u – Current username
# \v – BASH version
# \V – Extra information about the BASH version
# \w – Current working directory ($HOME is represented by ~)
# \W – The basename of the working directory ($HOME is represented by ~)
# \! – Lists this command’s number in the history
# \# – This command’s command number
# \$ – Specifies whether the user is root (#) or otherwise ($)
# \\– Backslash
# \[ – Start a sequence of non-displayed characters (useful if you want to add a command or instruction set to the prompt)
# \] – Close or end a sequence of non-displayed characters

# Color Changes
#
# • \e[ – Begin color changes
# • 0;32m – Specify the color code
# • [\u@\h \W]\$ – This is the code for your normal BASH prompt (username@hostname Workingdirectory $)
# • \e[0m – Exit color-change mode

# The first number in the color code specifies the typeface:
# • 0 – Normal
# • 1 – Bold (bright)
# • 2 – Dim
# • 4 – Underlined

# The second number indicates the color you want:

# • 30 – Black
# • 31 – Red
# • 32 – Green
# • 33 – Brown
# • 34 – Blue
# • 35 – Purple
# • 36 – Cyan
# • 37 – Light gray
```

## Configure the Bash Prompt to show current architecture
- Set bashrc to include a special prompt configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_prompt ]; then
		. ~/.bash_prompt
	fi
```

- Check settings
```bash
cat ~/.bash_prompt
	# Set the bash prompt
	# Example: (arch-shell)user@folder >
	if [ $(arch) == "i386" ]; then
		export PS1="(x86_64-$(basename $SHELL))\u@\w> "
	fi
```

- Check current shell architecture
```bash
arch
	arm64
```

- Launch x86 (Rosetta) shell
```bash
x86term
```

- Re-check current shell architecture
```bash
arch
	i386
```

- Set prompt for arm64 mode
```bash
export PS1="(x86_64-$(basename $SHELL))\u@\w> "
```

- The resulting shell prompt
```bash
(x86_64-bash)dxindustries@~>
```
