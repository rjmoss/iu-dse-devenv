# Working with PIP (x86_64)

## Checking and Updating PIP version
- Check Python
```bash
which python
    /Users/dxindustries/.asdf-x86_64/shims/python

python --version
    Python 2.7.18
```

- Check PIP
```bash
which pip
    /Users/dxindustries/.asdf-x86_64/shims/pip

pip --version
    pip 19.2.3 from /Users/dxindustries/.asdf-x86_64/installs/python/2.7.18/lib/python2.7/site-packages/pip (python 2.7)
```

- Update PIP
```bash
pip install --upgrade pip
```

## Managing PIP Packages
- Check base PIP packages (what's show are installed by default with python 2.7.18)
```bash
pip list	
	Package    Version
	---------- -------
	pip        20.3.4
	setuptools 41.2.20
```

## Uninstall all PIP packages
```bash
pip freeze | xargs pip uninstall -y
```

## Installing PIP Packages
- Locate Pip Packages
- Link: https://pypi.org/search

- Install required pip packages for this project
```bash
pip install numpy
```

