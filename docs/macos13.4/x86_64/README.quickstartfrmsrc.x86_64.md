# Source Controlled Project Quickstart (x86_64)

## Open a new Terminal Shell and switch to x86_64 mode
```bash
x86term
(x86_64-bash)dxindustries@~> 
```

## From base source (under source control)
- Base source
```bash
├── .git
├── .gitignore
├── .tool-versions-x86_64
├── numpy_test.py
└── requirements.txt

2 directories, 4 files
```

- Check the tool version
```bash
cat .tool-versions-x86_64
	python 2.7.18
```

- Create a virtual environment (named ```env```)
```bash
python -m virtualenv env
```

- Activate the virtual environment
```bash
source env/bin/activate
```

- Check the tool version
```bash
which python
	/Users/dxindustries/tmp/python2_project/env/bin/python
```

- List base packages in the environment
```bash
pip list
	Package    Version
	---------- -------
	pip        20.3.4
	setuptools 44.1.1
	wheel      0.37.1
```

- Install required packages from ```requirements.txt```
```bash
pip install -r requirements.txt
	Collecting numpy==1.16.6
	  Using cached numpy-1.16.6-cp27-cp27m-macosx_10_9_x86_64.whl (13.9 MB)
	Installing collected packages: numpy
	Successfully installed numpy-1.16.6
```

- Call python to test the program
```bash
python -m numpy_test
	[  33.    16.   657.   295.  7197.    23.5  140. ]
	[  -30.   -16.  -700.  -330. -7386.   -23.  -140.]
	7
	18
```

- Deactivate virtual environment
```bash
deactivate
```


