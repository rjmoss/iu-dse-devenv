# Mac OSX x86_64 Mode Command Line Aliases

## Notes

Homebrew manages installed packages separately from OS (or "system") package installations.  
There are separate versions required for packages that rely on x86_64 architecture (since the M1 is ARM).
In Python 2.7, some packages have not been patched to support the M1 (arm64) architecture and must be run in x86_64 compatible mode.

Even though Python 2.7 is deprecated, many python examples only run in Python 2.x (syntax) and a 2.x environment is needed to execute these examples.

## Setup Shell Aliases for Intel compatible mode
- See: https://stackoverflow.com/questions/75942145/how-to-quickly-change-between-arm64-or-x86-architecture-in-m1-m2-mac-terminals

- Set bashrc to include a special alias configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_aliases ]; then
		. ~/.bash_aliases
	fi
```

- Set aliases in current shell
```bash
alias x86term="/usr/bin/arch -x86_64 ${_shell} --login"
```

- Check settings
```bash
cat ~/.bash_aliases
	# Grab the shell from the current environment (should be /bin/bash)
	_shell=$SHELL

	# Alias for switching shell to Intel x86 mode emulation
	# Note: This has the same effect as running Terminal.app in Rosetta mode

	# x86 Intel Mode
	#alias x86term="$env /usr/bin/arch -x86_64 ${_shell} --login"
	alias x86term="/usr/bin/arch -x86_64 ${_shell} --login"

	echo "${_self}: use 'x86term' to switch to Intel processor compatible terminal mode."
```

- Check current shell architecture
```bash
arch
	arm64
```

- Launch x86 (Rosetta) shell
```bash
x86term
```

- Re-check current shell architecture
```bash
arch
	i386
```