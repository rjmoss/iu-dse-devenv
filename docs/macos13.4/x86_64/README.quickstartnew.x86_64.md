# New Project Quickstart (x86_64)

## Open a new Terminal Shell and switch to x86_64 mode
```bash
x86term
(x86_64-bash)dxindustries@~> 
```

## Create an initialize a new project
- Create a new project folder
```bash
mkdir -p ~/tmp/python2_project
pushd ~/tmp/python2_project
```

- Initialize local source control
```bash
git init .
```

- Set Project Python Version (ASDF)
```bash
asdf local python 2.7.18
```

- Check the tool version
```bash
cat .tool-versions-x86_64
	python 2.7.18
```

- Create a virtual environment (named ```env```)
```bash
python -m virtualenv env
```

- This creates a virtual python environment in the local folder
```bash
.
└── env
    ├── bin
    ├── include
    ├── lib
    └── pyvenv.cfg

5 directories, 1 file
```

- Activate the virtual environment
```bash
source env/bin/activate
```

- Check the tool version
```bash
which python
	/Users/dxindustries/tmp/python2_project/env/bin/python
```

- List base packages in the environment
```bash
pip list
	Package    Version
	---------- -------
	pip        20.3.4
	setuptools 44.1.1
	wheel      0.37.1
```

## Create a test application
- Create ```numpy_test.py``` python program
```bash
touch numpy_test.py
```

- Make the python source program executable
```bash
chmod a+x numpy_test.py
```

- Edit the file to match this source listing
```python
#!/usr/bin/env python2
# Note: Python2 print works without parentheses
import numpy as np
x = np.array([18, 8, 307, 130, 3504, 12, 70])
y = np.array([15, 8, 350, 165, 3693, 11.5, 70])

print x + y
print -2 * y
print x.size
print x[0]
```

- Install numpy in the virtual environment
```bash
pip install numpy
```

## Test/develop python program (module)
- Execute as a stand alone script
```bash
./numpy_test.py
	[  33.    16.   657.   295.  7197.    23.5  140. ]
	[  -30.   -16.  -700.  -330. -7386.   -23.  -140.]
	7
	18
```

- Call script from python
```bash
python -m numpy_test
	[  33.    16.   657.   295.  7197.    23.5  140. ]
	[  -30.   -16.  -700.  -330. -7386.   -23.  -140.]
	7
	18
```

- Snapshot the current PIP package list (for the host)
```bash
pip freeze > requirements.txt
```

- Check requirements
```bash
cat requirements.txt
	numpy==1.16.6
```

## Add Files to Source control
- Add Python version info (ASDF)
```bash
git add .tool-versions-x86_64
```

- Add the virtual environment to the gitignore
```bash
echo "env" >> .gitignore
```

- Add python compiled bytecode to the gitignore (this is specific to Python 2)
```bash
echo "*.pyc" >> .gitignore
```

- Add the gitignore file to the repository
```bash
git add .gitignore
```

- Add the test program to source control
```bash
git add numpy_test.py
```

- Add the pip package requirements.txt to source control
```bash
git add requirements.txt
```

- Check files
```bash
git status .
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   .gitignore
	new file:   .tool-versions-x86_64
	new file:   numpy_test.py
	new file:   requirements.txt
```

- Initial Commit
```bash
git commit -m "Initial project commit"
```

## Deactivate virtual environment
```bash
deactivate
```


