# ASDF Runtime Version Manager
- Link: https://asdf-vm.com/

## Notes

ASDF manages parallel installations of programming languages.
ASDF can be used with existing language specific library managers for each language.

For Data Science we will use ASDF plugins for Python and R version management that will allow us to use VIRTUALENV (for Python 2.x) and RENV for managing our Python and R versions on our development system.

Use this within Homebrew.

## Install and Configure Plugins
- Python Plugin
- Link: https://github.com/asdf-community/asdf-python

### Python 2
- Install system dependencies (including for pyenv)
- Note: Python 2 is deprecated, but may still be needed for old projects (i.e. IU Datascience Essentials Example Code)
- Note: MAC M1 processors (ARM64) have issues with older application/recipes that haven't been updated to support targeting M1 architecture
- Note: Python 2.7.x NUMPY is one of these packages.  When building, floating point altivec support for x86 fails (because it's not available).
- SOLUTION: In short, the solution is to enable Rosetta, which allows ARM based MACs to use apps built for Intel MAC architectures
- See: https://support.apple.com/en-us/HT211861
- See: https://alexslobodnik.medium.com/apple-m1-python-pandas-and-homebrew-20f14828ccc7

- Install system dependencies (in brew)
```bash
brew install openssl readline sqlite3 xz zlib tcl-tk pkgconfig
```

- Check for python plugin
```bash
asdf plugin-list-all | grep python
	python                        https://github.com/danhper/asdf-python.git
```

- Install Python Plugin
```bash
asdf plugin-add python
```

- List Python Versions
```bash
asdf list all python 2.
	...
	2.7.16
	2.7.17
	2.7.18
```

- Show latest stable Python Version
```bash
asdf latest python 2
	2.7.18
```

- Install A Specific Python 2 Version
- Note: Even though there is no "issue" installing without the brew tcl-tk installation, there are issues with MacOS Cocoa backend
- Note: Specifying to use the "system" (a.k.a. brew installed) tcl-tk resolves this problem and prevents issues with tkinter drawing black windows
```bash
PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='$(pkg-config tk --cflags)' --with-tcltk-libs='$(pkg-config tk --libs)' --enable-shared" asdf install python 2.7.18
```

- List Installed Python Versions
```bash
asdf list 
python
  2.7.18
```

- Set default global version
```bash
asdf global python 2.7.18
```

- Check default global version
```bash
asdf list
python
 *2.7.18
```

- Check that default global version is set in tool version file
```bash
cat ~/.tool-versions_x86_64
	python 2.7.18
```

### Verify Tkinter can generate a readable window
- Note: On MacOSX, Tkinter generated a window with black text on a black background
- Note: This was due to not using the correct ```tcl-tk``` version when installing python
- Added ```PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='$(pkg-config tk --cflags)' --with-tcltk-libs='$(pkg-config tk --libs)'``` to the install command

- Launch Python
```bash
python
```

- Import Tkinter and run the built-in test to generate a dialog box
- Note: Verify that the dialog is readable
```python
>>> import Tkinter
>>> Tkinter._test()
```

## Optional

### Set Global Python Environment Configuration
- Set bashrc to include a special homebrew configuration file
```bash
cat ~/.bash_profile
	...
	if [ -f ~/.bash_pythonenv ]; then
		. ~/.bash_pythonenv
	fi
```

- Disable Python writing of compiled bytecode files
- Note: In Python2 these are ```.pyc``` files in the same directory as the module source
- Check settings
```bash
cat ~/.bash_pythonenv
	# Disable Python Bytecode Files
	export PYTHONDONTWRITEBYTECODE=1
```