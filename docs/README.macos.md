# MacOS Ventura 13.4 Data Science Essentials Course Development Tool Setup

Indiana University's Data Science Essentials course contains many programming examples and exercises.
Course Link: https://expand.iu.edu/browse/sice/skillsets/programs/dse

This repository captures personal notes for installing and configuring development enviroments for completing coursework.

These notes were created and tested on the following system configuration:

1. MacOS Ventura 13.4
	- On a 14" Macbook Pro (2021) with Apple's ARM-based M1 silicon.

## Key Development Environment Notes
- Multi-version support for Python3 / Python2 on Macbook Pro M1 (arm64)
	- Some course-provided Python program examples are given for Python2 which has been deprecated
	- Some Python2 packages (numpy included) only work in i386 mode on M1 silicon (Python2 versions are not updated to run on M1 arm64 architecture)
	- Homebrew must also be run in i386 mode to install Python2 and use Numpy
	- Terminal will be configured for Rosetta/i386 emulation mode when developing with Python2

- Multi-version support for R on Macbook Pro M1
- Host package and dependency management with Homebrew and ASDF
- Python packages and dependency management with pip and virtual python environments

## Quickstart Overview


### Apple System and Tool Setup

#### Apple System Configuration

1. Change the Default Terminal Shell to BASH

macOS uses ```zsh``` by default.  The examples in this repository are based on a ```bash``` shell environment.

To switch macOS login shell from ```zsh``` to ```bash```:
- Open Spotlight Search using the key combination ```Command + <space_bar>```
- Search for and open ```Terminal.app```
- Change the default shell to bash
- Note: you will need to enter your password to complete the process
```bash
chsh -s /bin/bash
	Changing shell for <username>.
	Password for <username>:
```

- Check the default shell ```/bin/sh``` is now BASH
```bash
/bin/sh --version
	GNU bash, version 3.2.57(1)-release (arm64-apple-darwin22)
	Copyright (C) 2007 Free Software Foundation, Inc.
```

2. Base Development Tools
	
	- Install the Xcode command line tools (clang, gnu, etc... base developer support)
		- [README.xcode.arm64.md](./macos13.4/README.xcode.arm64.md)

#### Native (arm64) Mode Development

1. Base Python3 Development Environment

	- Create command line alias for arm64 architecture (Rosetta) mode bash session
		- [README.aliases.arm64.md](./macos13.4/arm64/README.aliases.arm64.md)
	
	- Set command line prompt to show current architecture in bash session prompt
		- [README.prompt.arm64.md](./macos13.4/arm64/README.prompt.arm64.md)

	- Install and configure homebrew 
		- [README.homebrew.arm64.md](./macos13.4/arm64/README.homebrew.arm64.md)

	- Install and configure ASDF
		- [README.asdf.arm64.md](./macos13.4/arm64/README.asdf.arm64.md)

	- Install and configure Python 3.x
		- [README.python3.arm64.md](./macos13.4/arm64/README.python3.arm64.md)

	- Upgrade and work with pip in Python 3.x
		- [README.pip3.arm64.md](./macos13.4/arm64/README.pip3.arm64.md)

	- Upgrade and work with venv virtual environments in Python 3.x
		- [README.python3.venv.arm64.md](./macos13.4/arm64/README.python3.venv.arm64.md)


#### Legacy (Intel x86_64) Mode Development (Rosetta)

1. Base Python2 Development Environment

	- Create command line alias for x86_64 architecture (Rosetta) mode bash session
		- [README.aliases.x86_64.md](./macos13.4/x86_64/README.aliases.x86_64.md)
	
	- Set command line prompt to show current architecture in bash session prompt
		- [README.prompt.x86_64.md](./macos13.4/x86_64/README.prompt.x86_64.md)

	- Install and configure homebrew 
		- [README.homebrew.x86_64.md](./macos13.4/x86_64/README.homebrew.x86_64.md)

	- Install and configure ASDF
		- [README.asdf.x86_64.md](./macos13.4/x86_64/README.asdf.x86_64.md)

	- Install and configure Python 2.x
		- [README.python2.x86_64.md](./macos13.4/x86_64/README.python2.x86_64.md)

	- Upgrade and work with pip in Python 2.x
		- [README.pip2.x86_64.md](./macos13.4/x86_64/README.pip2.x86_64.md)

	- Install and configure Python 2.x Virtual Environments (PIP2)
		- [README.pip2.virtualenv.x86_64.md](./macos13.4/x86_64/README.pip2.virtualenv.x86_64.md)


### New Project Workflow Examples

#### Native (arm64) Mode Development Workflow

1. Python3 Project Workflow

	- Create and Configure a New Project [README.quickstartnew.arm64.md](./macos13.4/arm64/README.quickstartnew.arm64.md)
	- Work with Existing Projects (source control) [README.quickstartsrc.arm64.md](./macos13.4/arm64/README.quickstartsrc.arm64.md)

#### Legacy (Intel x86_64) Mod Development Workflow

1. Python2 Project Workflow

	- Create and Configure a New Project [README.quickstartnew.x86_64.md](./macos13.4/x86_64/README.quickstartnew.x86_64.md)
	- Work with Existing Projects (source control) [README.quickstartsrc.x86_64.md](./macos13.4/x86_64/README.quickstartsrc.x86_64.md)
README.macos.md
