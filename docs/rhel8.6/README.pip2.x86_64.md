# Working with PIP2

## Checking and Updating PIP version
- Check Python
```bash
which python
    ~/.asdf_0.12.0/shims/python

python --version
    Python 2.7.18
```

- Check PIP
```bash
which pip
    ~/.asdf_0.12.0/shims/pip

pip --version
    pip 19.2.3 from /home/rjmoss/.asdf_0.12.0/installs/python/2.7.18/lib/python2.7/site-packages/pip (python 2.7)
```

- Update PIP
```bash
pip install --upgrade pip
```

## Managing PIP Packages
- Check base PIP packages (what's show are installed by default with python 2.7.18)
```bash
pip list	
	Package    Version
	---------- -------
	pip        20.3.4
	setuptools 41.2.0
```

## Uninstall all PIP packages
```bash
pip freeze | xargs pip uninstall -y
```

## Installing PIP Packages
- Locate Pip Packages
- Link: https://pypi.org/search

- Install required pip packages for this project
```bash
pip install numpy
```

