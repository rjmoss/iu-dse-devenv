# ASDF Runtime Version Manager
- Link: https://asdf-vm.com/

## Notes

ASDF manages parallel installations of programming languages.
ASDF can be used with existing language specific library managers for each language.

For Data Science we will use ASDF plugins for Python and R version management that will allow us to use VIRTUALENV and RENV for managing our Python and R versions on our development system.

ASDF will be built from Source.

## Install and Configure ASDF from Source
- Install asdf dependencies
```bash
dnf install curl git
```

- Clone the ASDF Repository
```bash
cd ~/repositories/github
mkdir -p asdf-vm/asdf/0.12.0/
cd asdf-vm/asdf/0.12.0
git clone https://github.com/asdf-vm/asdf.git . --branch v0.12.0
```

- Set path to use asdf from command line
```bash
source ~/repositories/github/asdf-vm/asdf/0.12.0/asdf.sh"
```

- Setup bash completion for asdf
```bash
source ~/repositories/github/asdf-vm/asdf/0.12.0/completions/asdf.bash"
```

- Set bashrc to include a special asdf configuration file
```bash
cat ~/.bashrc
	...
	if [ -f ~/.bash_asdf ]; then
		. ~/.bash_asdf
	fi
```
- Check settings
```bash
cat ~/.bash_homebrew_asdf
# ASDF Configuration
TOOL_VERSION=0.12.0
TOOL_PATH=/home/rjmoss/repositories/github/asdf-vm/asdf/${TOOL_VERSION}

echo "${_self}: ASDF ${TOOL_VERSION} using default configuration for x86_64 architecture"

# Override the default tool version filename to indicate native host architecture
#export ASDF_DEFAULT_TOOL_VERSIONS_FILENAME=.tool-versions

# Set ASDF Configuration for running architecture mode
export ASDF_CONFIG_FILE=${HOME}/.asdfrc_0.12.0
export ASDF_DATA_DIR=${HOME}/.asdf_0.12.0
export ASDF_DIR=${TOOL_PATH}

# ASDF
if [ -e "${TOOL_PATH}/asdf.sh" ]; then
	echo "${_self}: using x86_64 asdf"
	source "${TOOL_PATH}/asdf.sh"
	source "${TOOL_PATH}/completions/asdf.bash"
else
	echo "${_self}: no x86_64 asdf detected!"
fi
```

- Verify path
```bash
which asdf
	asdf ()
	{ 
	    case $1 in 
	        "shell")
	            if ! shift; then
	                printf '%s\n' 'asdf: Error: Failed to shift' 1>&2;
	                return 1;
	            fi;
	            eval "$(asdf export-shell-version sh "$@")"
	        ;;
	        *)
	            command asdf "$@"
	        ;;
	    esac
	}
```

