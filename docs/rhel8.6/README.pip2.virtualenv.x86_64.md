# Working with PIP and Virtual Environments

Python 2 uses the ```virtualenv``` python package

## Install and configure virtualenv
- Check version
```bash
python --version
	Python 2.7.18
```

- Check PIP
```bash
pip --version
	pip 20.3.4 from /home/rjmoss/.asdf_0.12.0/installs/python/2.7.18/lib/python2.7/site-packages/pip (python 2.7)
```

- Install Virtualenv
```bash
pip install virtualenv
```

- Show installed packages
```bash
pip list
	Package             Version
	------------------- -----------
	configparser        4.0.2
	contextlib2         0.6.0.post1
	distlib             0.3.6
	filelock            3.2.1
	importlib-metadata  2.1.3
	importlib-resources 3.3.1
	pathlib2            2.3.7.post1
	pip                 20.3.4
	platformdirs        2.0.2
	scandir             1.10.0
	setuptools          41.2.0
	singledispatch      3.7.0
	six                 1.16.0
	typing              3.10.0.0
	virtualenv          20.15.1
	zipp                1.2.0
```
