# ASDF Runtime Version Manager
- Link: https://asdf-vm.com/

## Notes

ASDF manages parallel installations of programming languages.
ASDF can be used with existing language specific library managers for each language.

For Data Science we will use ASDF plugins for Python and R version management that will allow us to use VIRTUALENV (for Python 2.x) and RENV for managing our Python and R versions on our development system.

Use this within Homebrew.

## Install and Configure Plugins
- Python Plugin
- Link: https://github.com/asdf-community/asdf-python

### Python 2
- Install system dependencies (including for pyenv)
- Note: Python 2 is deprecated, but may still be needed for old projects (i.e. IU Datascience Essentials Example Code)

- Install system dependencies
```bash
sudo dnf install openssl readline readline-devel sqlite sqlite-devel xz zlib tcl
```

- Check for python plugin
```bash
asdf plugin-list-all | grep python
	python                        https://github.com/danhper/asdf-python.git
```

- Install Python Plugin
```bash
asdf plugin-add python
```

- List Python Versions
```bash
asdf list all python 2.
	...
	2.7.16
	2.7.17
	2.7.18
```

- Show latest stable Python Version
```bash
asdf latest python 2
	2.7.18
```

- Install A Specific Python 2 Version
```bash
env PYTHON_CONFIGURE_OPTS="--enable-shared" asdf install python 2.7.18
```

- List Installed Python Versions
```bash
asdf list 
python
  2.7.18
```

- Set default global version
```bash
asdf global python 2.7.18
```

- Check default global version
```bash
asdf list
python
 *2.7.18
```

- Check that default global version is set in tool version file
```bash
cat ~/.tool-versions
	python 2.7.18
```
