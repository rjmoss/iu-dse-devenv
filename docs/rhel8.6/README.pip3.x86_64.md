# Working with PIP3

## Checking and Updating PIP version
- Check Python
```bash
which python
    ~/.asdf_0.12.0/shims/python

python --version
    Python 3.11.4
```

- Check PIP
```bash
which pip
    ~/.asdf-arm64/shims/pip

pip --version
    pip 23.1.2 from /home/rjmoss/.asdf_0.12.0/installs/python/3.11.4/lib/python3.11/site-packages/pip (python 3.11)
```

- Update PIP
```bash
pip install --upgrade pip
```

## Managing PIP Packages
- Check base PIP packages (what's show are installed by default with python 2.7.18)
```bash
pip list	
	Package    Version
	---------- -------
	pip        23.1.2
	setuptools 65.5.0
```

## Uninstall all PIP packages
```bash
pip freeze | xargs pip uninstall -y
```

## Installing PIP Packages
- Locate Pip Packages
- Link: https://pypi.org/search

- Install required pip packages for this project
```bash
pip install numpy
```

