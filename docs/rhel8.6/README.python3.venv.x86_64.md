# Working with PIP3 and Virtual Environments

Python3 uses built-in ```venv``` support

## Check built-in venv
- Check version
```bash
python --version
	Python 3.11.4
```

- Check that the venv module is present
```bash
python3 -m venv -h
	usage: venv [-h] [--system-site-packages] [--symlinks | --copies] [--clear] [--upgrade] [--without-pip]
	            [--prompt PROMPT] [--upgrade-deps]
	            ENV_DIR [ENV_DIR ...]

	Creates virtual Python environments in one or more target directories.

	positional arguments:
	  ENV_DIR               A directory to create the environment in.

	options:
	  -h, --help            show this help message and exit
	  --system-site-packages
	                        Give the virtual environment access to the system site-packages dir.
	  --symlinks            Try to use symlinks rather than copies, when symlinks are not the default for the
	                        platform.
	  --copies              Try to use copies rather than symlinks, even when symlinks are the default for the
	                        platform.
	  --clear               Delete the contents of the environment directory if it already exists, before
	                        environment creation.
	  --upgrade             Upgrade the environment directory to use this version of Python, assuming Python has
	                        been upgraded in-place.
	  --without-pip         Skips installing or upgrading pip in the virtual environment (pip is bootstrapped by
	                        default)
	  --prompt PROMPT       Provides an alternative prompt prefix for this environment.
	  --upgrade-deps        Upgrade core dependencies: pip setuptools to the latest version in PyPI

	Once an environment has been created, you may wish to activate it, e.g. by sourcing an activate script in its
	bin directory.
```

