# RedHat Linux 8.6 Data Science Essentials Course Development Tool Setup

Indiana University's Data Science Essentials course contains many programming examples and exercises.
Course Link: https://expand.iu.edu/browse/sice/skillsets/programs/dse

This repository captures personal notes for installing and configuring development enviroments for completing coursework.

These notes were created and tested on the following system configuration:

1. Redhat Linux 8.6
	- On a Custom AMD Ryzen 5950x / Socket AM X570 based-system.

## Key Development Environment Notes
- Multi-version support for Python3 / Python2 on AMD Ryzen 5950x
	- Some course-provided Python program examples are given for Python2 which has been deprecated

- Multi-version support for R on AMD Ryzen 5950x
- Host package and dependency management with ASDF
- Python packages and dependency management with pip and virtual python environments

## Quickstart Overview


### System and Tool Setup

#### System Configuration

#### Native (Intel x86_64) Mode Development (Rosetta)

1. Base Python Development Environment (Python2 and Python3)

	- Install and configure ASDF
		- [README.asdf.x86_64.md](./rhel8.6/README.asdf.x86_64.md)

	- Install and configure Python 2.x
		- [README.python2.x86_64.md](./macos13.4/x86_64/README.python2.x86_64.md)

	- Upgrade and work with pip in Python 2.x
		- [README.pip2.x86_64.md](./macos13.4/x86_64/README.pip2.x86_64.md)

	- Install and configure Python 2.x Virtual Environments (PIP2)
		- [README.pip2.virtualenv.x86_64.md](./macos13.4/x86_64/README.pip2.virtualenv.x86_64.md)

	- Install and configure Python 3.x
		- [README.python3.arm64.md](./macos13.4/arm64/README.python3.arm64.md)

	- Upgrade and work with pip in Python 3.x
		- [README.pip3.arm64.md](./macos13.4/arm64/README.pip3.arm64.md)

	- Upgrade and work with venv virtual environments in Python 3.x
		- [README.python3.venv.arm64.md](./macos13.4/arm64/README.python3.venv.arm64.md)

### New Project Workflow Examples

#### Native (arm64) Mode Development Workflow

1. Python3 Project Workflow

	- Create and Configure a New Project [README.quickstartnew.arm64.md](./macos13.4/arm64/README.quickstartnew.arm64.md)
	- Work with Existing Projects (source control) [README.quickstartsrc.arm64.md](./macos13.4/arm64/README.quickstartsrc.arm64.md)

#### Legacy (Intel x86_64) Mod Development Workflow

1. Python2 Project Workflow

	- Create and Configure a New Project [README.quickstartnew.x86_64.md](./macos13.4/x86_64/README.quickstartnew.x86_64.md)
	- Work with Existing Projects (source control) [README.quickstartsrc.x86_64.md](./macos13.4/x86_64/README.quickstartsrc.x86_64.md)
README.macos.md
